import React, { memo, useEffect } from "react";

export function Banner({ title, handleClick }) {
  // Banner(props) (1)
  useEffect(() => {
    return () => {
      console.log("Will unmount");
    };
  }, []);
  console.log("Banner render");
  return (
    <div className="p-5 bg-dark text-white">
      Banner
      <h2>{title}</h2>
      {/* <h2>{props.title}</h2> (1) */}
      <button onClick={handleClick} className="btn btn-light">
        Plus like
      </button>
    </div>
  );
}
export default memo(Banner);
