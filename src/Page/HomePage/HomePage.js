import axios from "axios";
import React, { Component } from "react";
import { TOKEN_CYBERSOFT } from "../utils/utils";
import MovieItem from "./MovieItem";

export default class HomePage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ movieArr: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderMovieList = () => {
    return this.state.movieArr.map((item, index) => {
      return (
        <div className="col-3" key={index}>
          <MovieItem movie={item} />;
        </div>
      );
    });
  };

  render() {
    console.log(this.state.movieArr);
    return (
      <div className="container">
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
