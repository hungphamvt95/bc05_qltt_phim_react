import logo from "./logo.svg";
import "./App.css";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter } from "react-router-dom";
import { Route, Switch } from "react-router";
import DetailPage from "./Page/DetailPage/DetailPage";
import { Component } from "react";
import "antd/dist/reset.css";
import DemoHookPage from "./Page/DemoHookPage/DemoHookPage";
import Header from "./Components/Header/Header";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/detail/:id" component={DetailPage} />
          <Route
            path="/hook"
            render={() => {
              return <DemoHookPage />; // dùng để truyền dữ liệu từ app vào nếu có
            }}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
